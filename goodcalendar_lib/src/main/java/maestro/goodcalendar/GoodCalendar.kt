package maestro.goodcalendar

import android.annotation.SuppressLint
import android.annotation.TargetApi
import android.content.Context
import android.content.res.ColorStateList
import android.graphics.*
import android.graphics.drawable.Drawable
import android.graphics.drawable.RippleDrawable
import android.graphics.drawable.ShapeDrawable
import android.graphics.drawable.StateListDrawable
import android.graphics.drawable.shapes.OvalShape
import android.os.Build
import android.support.v4.content.res.ResourcesCompat
import android.support.v4.view.GravityCompat
import android.support.v4.view.MotionEventCompat
import android.support.v4.view.ViewCompat
import android.support.v7.widget.AppCompatCheckedTextView
import android.support.v7.widget.AppCompatTextView
import android.util.AttributeSet
import android.util.TypedValue
import android.view.*
import android.widget.CheckedTextView
import android.widget.ImageView
import android.widget.TextView
import java.text.SimpleDateFormat
import java.util.*

/**
 * Created by a.barouski on 8/30/2017.
 */
class GoodCalendar : ViewGroup {

    companion object {
        private val PENDING_NONE = 0
        private val PENDING_NEXT = 1
        private val PENDING_PREVIOUS = 2

        private val calendar = Calendar.getInstance()!!
    }

    var calendarsCount = 1
        set(value) {
            field = value
            requestLayout()
            minCalendarCache = calendarsCount + 2
        }
    var minCalendarCache = calendarsCount + 2

    var formatter = DateFormatter()
        set(value) {
            field = value
            //TODO: synchronize?
        }

    var calendarSpace = 0
        set(value) {
            field = value
            requestLayout()
        }

    var monthTitleSpace = 0
        set(value) {
            field = value
            requestLayout()
        }

    var weekDaysTitleSpace = 0
        set(value) {
            field = value
            requestLayout()
        }

    var monthTitleLayout = R.layout.def_calendar_month_title_view
        private set
    var weekDayTitleLayout = R.layout.def_calendar_week_day_title_view
        private set
    var dayLayout = R.layout.def_calendar_day_view
        private set
    var arrowLayout = R.layout.def_calendar_switch_button_view
        private set

    var mode: SelectionMode = MultipleMode(this)//RangeMode(this)//SimpleMode(this)
        set(value) {
            field = value
            //TODO: synchronize
        }

    var tintColor = -1
        set(value) {
            field = value
            monthViews.forEach {
                it?.updateTintColor(this, value)
            }
        }

    private var maxDate: SelectedDate? = null
    private var minDate: SelectedDate? = null

    //0 - left scroll offset , size-1 -> right scroll offset
    private val monthViews by lazy { Array<CalendarMonthView?>(minCalendarCache, { i -> null }) }

    private lateinit var dragHelper: SimpleDragHelper

    private var canSlide = true
    private var isUnableToDrag = false

    private var pendingChange = PENDING_NONE

    private val longPressTimeOut: Long
    private var actionDownTime: Long = 0
    private var currentTranslationOffset = 0F
    private var initialMotionX: Float = 0.toFloat()
    private var initialMotionY: Float = 0.toFloat()
    private var isDisallowInterceptTouchRequested = false

    var leftArrow: ImageView? = null
        private set
    var rightArrow: ImageView? = null
        private set

    constructor(context: Context) : this(context, null)

    constructor(context: Context, attrs: AttributeSet?) : this(context, attrs, 0)

    constructor(context: Context, attrs: AttributeSet?, defStyle: Int) : super(context, attrs, defStyle) {
        clipChildren = false
        clipToPadding = false
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            clipToOutline = false
        }

        dragHelper = SimpleDragHelper(context, this, dragHelperCallback)// DragHelper.create(this, 0.45F, mDragCallback)
        calendarSpace = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 8F, resources.displayMetrics).toInt()
        longPressTimeOut = ViewConfiguration.getLongPressTimeout().toLong()
        monthTitleSpace = calendarSpace
        weekDaysTitleSpace = calendarSpace

        if (attrs != null) {
            val array = context.obtainStyledAttributes(attrs, R.styleable.GoodCalendar)
            calendarsCount = array.getInt(R.styleable.GoodCalendar_calendarsCount, calendarsCount)
            calendarSpace = array.getDimensionPixelSize(R.styleable.GoodCalendar_calendarSpace, calendarSpace)
            monthTitleLayout = array.getResourceId(R.styleable.GoodCalendar_monthTitleLayout, monthTitleLayout)
            weekDayTitleLayout = array.getResourceId(R.styleable.GoodCalendar_weekDayTitleLayout, weekDayTitleLayout)
            dayLayout = array.getResourceId(R.styleable.GoodCalendar_dayLayout, dayLayout)
            monthTitleSpace = array.getDimensionPixelSize(R.styleable.GoodCalendar_monthTitleSpace, monthTitleSpace)
            weekDaysTitleSpace = array.getDimensionPixelSize(R.styleable.GoodCalendar_weekDaysTitleSpace, weekDaysTitleSpace)
            tintColor = if (!array.hasValue(R.styleable.GoodCalendar_tintColor)) {
                getThemeAccentColor(context)
            } else {
                array.getColor(R.styleable.GoodCalendar_tintColor, 0)
            }
            array.recycle()
        }
        for (i in 0 until minCalendarCache) {
            monthViews[i] = CalendarMonthView(context).also {
                it.prepare(this)
                addView(it)
            }
        }

        with(LayoutInflater.from(context)) {
            leftArrow = (inflate(arrowLayout, this@GoodCalendar, false) as? ImageView)?.also {
                it.setOnClickListener { jumpPrevious() }
                addView(it)
            }
            rightArrow = (inflate(arrowLayout, this@GoodCalendar, false) as? ImageView)?.also {
                it.setOnClickListener { jumpNext() }
                addView(it)
            }
        }
        setArrowDrawables(
                ResourcesCompat.getDrawable(resources, R.drawable.ic_navigate_before_black_24dp, context.theme),
                ResourcesCompat.getDrawable(resources, R.drawable.ic_navigte_forward_black_24dp, context.theme))

        resetCalendarAlphas(0F)
        prepareCalendar(monthViews[0], calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH) - 1)
        prepareCalendar(monthViews[1], calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH))
        for (i in 2 until monthViews.size) {
            prepareCalendar(monthViews[i], calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH) + i - 1)
        }
    }

    fun setArrowDrawables(previous: Drawable?, next: Drawable?) {
        leftArrow?.setImageDrawable(previous)
        rightArrow?.setImageDrawable(next)
    }

    fun setLimits(minDate: Date?, maxDate: Date?) {
        this.minDate = SelectedDate.fromDate(minDate)
        this.maxDate = SelectedDate.fromDate(maxDate)
        syncCheckedState()
    }

    fun canScrollToNext(): Boolean {
        if (maxDate == null) return true
        val monthView = monthViews[1]!!
        return maxDate!!.compare(monthView.year, monthView.month) == SelectedDate.GRATER
    }

    fun canScrollToPrevious(): Boolean {
        if (minDate == null) return true
        val monthView = monthViews[1]!!
        return minDate!!.compare(monthView.year, monthView.month) == SelectedDate.LOWER
    }

    override fun dispatchDraw(canvas: Canvas?) {
        if (canvas == null) return
        canvas.save()
        canvas.translate(currentTranslationOffset, 0F)
        super.dispatchDraw(canvas)
        canvas.restore()
    }

    override fun onLayout(changed: Boolean, l: Int, t: Int, r: Int, b: Int) {
        if (width == 0 || height == 0) return
        var left = paddingLeft
        val top = paddingTop
        monthViews.first()?.let {
            it.layout(left - it.measuredWidth - calendarSpace, top, left - calendarSpace, top + it.measuredHeight)
        }
        for (i in 1..calendarsCount) {
            monthViews[i]?.let {
                it.layout(left, top, left + it.measuredWidth, top + it.measuredHeight)
                left += it.measuredWidth + calendarSpace
            }
        }
        monthViews.last()?.let {
            it.layout(left, top, left + it.measuredWidth, top + it.measuredHeight)
        }
        leftArrow?.let {
            val top = monthViews[1]?.monthTitleView?.measuredHeight?.div(2)?.minus(it.measuredHeight / 2) ?: top
            val left = paddingLeft - it.measuredWidth / 2 + it.drawable.intrinsicWidth / 2
            it.layout(left, top, left + it.measuredWidth, top + it.measuredHeight)
        }
        rightArrow?.let {
            val top = monthViews[monthViews.size - 2]?.monthTitleView?.measuredHeight?.div(2)?.minus(it.measuredHeight / 2) ?: top
            val left = width - paddingRight - it.measuredWidth / 2 - it.drawable.intrinsicWidth / 2
            it.layout(left, top, left + it.measuredWidth, top + it.measuredHeight)
        }
    }

    var isResizeAllowed = true
    var initialHeightMode: Int? = null

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        if (initialHeightMode == null) {
            initialHeightMode = heightMeasureSpec
        }
        var availableSize = MeasureSpec.getSize(widthMeasureSpec)
        if (MeasureSpec.getMode(initialHeightMode!!) == MeasureSpec.EXACTLY) {
            availableSize = minOf(availableSize, MeasureSpec.getSize(initialHeightMode!!) - paddingTop - paddingBottom)
        }
        if (availableSize > 0) {
            availableSize = (availableSize - paddingLeft - paddingRight - (calendarSpace * (calendarsCount - 1))) / calendarsCount
        }
        if (leftArrow != null) measureChild(leftArrow, widthMeasureSpec, heightMeasureSpec)
        if (rightArrow != null) measureChild(rightArrow, widthMeasureSpec, heightMeasureSpec)
        val measureResults = measure(widthMeasureSpec, heightMeasureSpec, availableSize)

        setMeasuredDimension(maxOf(measureResults[0], availableSize),
                if (
                MeasureSpec.getMode(initialHeightMode!!) != MeasureSpec.EXACTLY && (
                        MeasureSpec.getMode(heightMeasureSpec) == MeasureSpec.EXACTLY || (isResizeAllowed && currentTranslationOffset != 0F && dragHelper.dragState != SimpleDragHelper.STATE_IDLE))) MeasureSpec.getSize(heightMeasureSpec) else measureResults[1])
    }

    private val tmpArray = IntArray(2)

    private var initialLayoutParamsHeight: Int? = null

    private fun measure(widthMeasureSpec: Int, heightMeasureSpec: Int, availableSize: Int): IntArray {

        var maxWidth = 0
        var maxHeight = 0
        var totalWidth = 0
        monthViews.forEachIndexed { index, item ->
            item?.let {
                if (availableSize > 0) {
                    it.measure(MeasureSpec.makeMeasureSpec(availableSize, MeasureSpec.EXACTLY), heightMeasureSpec)
                } else {
                    measureChild(it, widthMeasureSpec, heightMeasureSpec)
                }
                if (index in 1..(calendarsCount - 1)) {
                    if (totalWidth > 0) {
                        totalWidth += calendarSpace
                    }
                    totalWidth += it.measuredWidth
                } else {
                    totalWidth = it.measuredWidth
                }
                maxWidth = maxOf(maxWidth, totalWidth)
                if (index > 0 && index < monthViews.size - 1) {
                    maxHeight = maxOf(maxHeight, it.measuredHeight)
                }
            }
        }
        tmpArray[0] = maxWidth + paddingLeft + paddingRight
        tmpArray[1] = maxHeight + paddingTop + paddingBottom
        return tmpArray
    }

    override fun requestDisallowInterceptTouchEvent(disallowIntercept: Boolean) {
        super.requestDisallowInterceptTouchEvent(disallowIntercept)
        isDisallowInterceptTouchRequested = disallowIntercept
    }

    private var initialLayoutParams: LayoutParams? = null

    override fun setLayoutParams(params: LayoutParams?) {
        if (initialLayoutParams != params) {
            initialLayoutParams = params
            initialLayoutParamsHeight = null
            initialHeightMode = null
        }
        if (initialLayoutParamsHeight == null) {
            initialLayoutParamsHeight = params?.height
        }
        super.setLayoutParams(params)
    }

    override fun onInterceptTouchEvent(ev: MotionEvent): Boolean {
        val action = MotionEventCompat.getActionMasked(ev)

        if (!canSlide || isUnableToDrag && action != MotionEvent.ACTION_DOWN) {
            dragHelper.cancel()
            return super.onInterceptTouchEvent(ev)
        }

        if (action == MotionEvent.ACTION_CANCEL || action == MotionEvent.ACTION_UP) {
            dragHelper.cancel()
            return false
        }

        when (action) {
            MotionEvent.ACTION_DOWN -> {
                val x = ev.x
                val y = ev.y
                isUnableToDrag = false
                initialMotionX = x
                initialMotionY = y
                actionDownTime = System.currentTimeMillis()
                isDisallowInterceptTouchRequested = false
            }
            MotionEvent.ACTION_MOVE -> {
                if (isDisallowInterceptTouchRequested) {
                    isDisallowInterceptTouchRequested = false
                    return false
                }
                val x = ev.x
                val y = ev.y
                val adx = Math.abs(x - initialMotionX)
                val ady = Math.abs(y - initialMotionY)
                val slop = dragHelper.mTouchSlop
                if (adx > slop && ady > adx) {
                    dragHelper.cancel()
                    isUnableToDrag = true
                    return false
                }
            }
        }

        return dragHelper.shouldInterceptTouchEvent(ev)
    }

    @SuppressLint("ClickableViewAccessibility")
    override fun onTouchEvent(ev: MotionEvent): Boolean {
        if (!canSlide) return super.onTouchEvent(ev)
        dragHelper.processTouchEvent(ev)
        return true
    }

    override fun computeScroll() {
        if (dragHelper.continueSettling(true)) {
            if (!canSlide) {
                dragHelper.abort()
                return
            }
            ViewCompat.postInvalidateOnAnimation(this)
        }
    }

    fun syncCheckedState() = monthViews.forEach { it?.syncCheckedState(this) }

    private fun prepareCalendar(view: CalendarMonthView?, year: Int, month: Int) = view?.setup(this, year, month)

    private fun resetCalendarAlphas(alpha: Float) {
        if (calendarsCount > 1) {
            monthViews[1]?.alpha = 1f
            monthViews[monthViews.size - 2]?.alpha = 1f
        }
        monthViews.first()?.alpha = alpha
        monthViews.last()?.alpha = alpha
    }

    fun jumpNext() {
        if (canScrollToNext() && dragHelper.animateSettle(0, 0, -monthViews[monthViews.size - 1]!!.measuredWidth - calendarSpace, 0)) {
            pendingChange = PENDING_NEXT
            invalidate()
        }
    }

    fun jumpPrevious() {
        if (canScrollToPrevious() && dragHelper.animateSettle(0, 0, monthViews[0]!!.measuredWidth + calendarSpace, 0)) {
            pendingChange = PENDING_PREVIOUS
            invalidate()
        }
    }

    fun next(): Boolean {
        if (!canScrollToNext()) return false
        resetCalendarAlphas(1f)
        val tmp = monthViews[0]
        for (i in 0..monthViews.size - 2) {
            monthViews[i] = monthViews[i + 1]
        }
        monthViews[monthViews.size - 1] = tmp
        monthViews[monthViews.size - 2]?.let {
            prepareCalendar(tmp, it.year, it.month + 1)
        }
//        rightArrow?.isEnabled = canScrollToNext()
        resetCalendarAlphas(0F)
        requestLayout()
        invalidate()
        return true
    }

    fun previous(): Boolean {
        if (!canScrollToPrevious()) return false
        resetCalendarAlphas(1F)
        val tmp = monthViews[monthViews.size - 1]
        tmp?.alpha = 1f
        for (i in monthViews.size - 1 downTo 1) {
            monthViews[i] = monthViews[i - 1]
        }
        monthViews[0] = tmp
        monthViews[1]?.let {
            prepareCalendar(tmp, it.year, it.month - 1)
        }
//        leftArrow?.isEnabled = canScrollToPrevious()
        resetCalendarAlphas(0F)
        requestLayout()
        invalidate()
        return true
    }

    private fun getThemeAccentColor(context: Context): Int {
        val colorAttr: Int = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            android.R.attr.colorAccent
        } else {
            context.resources.getIdentifier("colorAccent", "attr", context.packageName)
        }
        val outValue = TypedValue()
        context.theme.resolveAttribute(colorAttr, outValue, true)
        return outValue.data
    }

    fun isDateEnabled(year: Int, month: Int, day: Int): Boolean {
        if (minDate != null && maxDate != null) return minDate!!.isLowerOrSame(year, month, day) && maxDate!!.isGraterOrSame(year, month, day)
        if (minDate != null) return minDate!!.isLowerOrSame(year, month, day)
        if (maxDate != null) return maxDate!!.isGraterOrSame(year, month, day)
        return true
    }

    private val dragHelperCallback by lazy {
        object : SimpleDragHelper.SimpleDragHelperCallback() {

            var initialHeight: Int? = null

            override fun onPositionChanged(changedView: View, dx: Int, dy: Int, idx: Int, idy: Int) {
                super.onPositionChanged(changedView, dx, dy, idx, idy)
                if (dx > 0 && !canScrollToPrevious()) {
                    currentTranslationOffset = 0F
                    dragHelper.forceSetDxDy(0, 0)
                } else if (dx < 0 && !canScrollToNext()) {
                    currentTranslationOffset = 0F
                    dragHelper.forceSetDxDy(0, 0)
                } else {
                    currentTranslationOffset += idx
                }
                val range = getClampHorizontalDragRange(changedView, currentTranslationOffset, 0F)
                if (Math.abs(currentTranslationOffset) > range) {
                    currentTranslationOffset = 0F
                    dragHelper.forceSetDxDy(0, 0)
                    if (idx > 0) previous() else next()
                    initialHeight = null
                }
                val offsetPercent = minOf(1f, Math.abs(currentTranslationOffset) / range)
                if (currentTranslationOffset > 0) {
                    monthViews.first()?.alpha = offsetPercent
                    if (calendarsCount > 1) monthViews[monthViews.size - 2]?.alpha = 1f - offsetPercent
                    monthViews.last()?.alpha = 0F
                } else {
                    monthViews.first()?.alpha = 0F
                    if (calendarsCount > 1) monthViews[1]?.alpha = 1f - offsetPercent
                    monthViews.last()?.alpha = offsetPercent
                }
                leftArrow?.translationX = -currentTranslationOffset
                rightArrow?.translationX = -currentTranslationOffset
                if (isResizeAllowed) {
                    val maxTargetHeight = getClampVerticalHeight(changedView, currentTranslationOffset, 0F)
                    if (initialHeight == null) {
                        initialHeight = measuredHeight
                    }
                    if (maxTargetHeight != measuredHeight) {
                        layoutParams.height = (initialHeight!! + offsetPercent * (maxTargetHeight - initialHeight!!)).toInt()
                        layoutParams = layoutParams
                    }
                }
                invalidate()
            }

            override fun onViewReleased(releasedChild: View, dx: Int, dy: Int, xvel: Float, yvel: Float) {
                super.onViewReleased(releasedChild, dx, dy, xvel, yvel)
                val horizontalRange = getClampHorizontalDragRange(releasedChild, currentTranslationOffset, 0F)
                if (currentTranslationOffset > 0 && (xvel > 0 || Math.abs(currentTranslationOffset) >= horizontalRange / 2)) {
                    pendingChange = PENDING_PREVIOUS
                    dragHelper.settleCapturedViewAt(currentTranslationOffset.toInt(), dy, horizontalRange, 0)
                } else if (currentTranslationOffset < 0 && (xvel < 0 || Math.abs(currentTranslationOffset) >= horizontalRange / 2)) {
                    pendingChange = PENDING_NEXT
                    dragHelper.settleCapturedViewAt(currentTranslationOffset.toInt(), dy, -horizontalRange, 0)
                } else {
                    pendingChange = PENDING_NONE
                    dragHelper.settleCapturedViewAt(currentTranslationOffset.toInt(), dy, 0, 0)
                }
                invalidate()
            }

            override fun onViewDragStateChanged(state: Int) {
                super.onViewDragStateChanged(state)
                if (state == SimpleDragHelper.STATE_IDLE) {
                    currentTranslationOffset = 0F
                    if (isResizeAllowed) {
                        layoutParams.height = initialLayoutParamsHeight!!
                        initialHeight = null
                    }
                    leftArrow?.translationX = 0F
                    rightArrow?.translationX = 0F
                    if (pendingChange == PENDING_NEXT) {
                        next()
                    } else if (pendingChange == PENDING_PREVIOUS) {
                        previous()
                    }
                    pendingChange = PENDING_NONE
                } else if (state == SimpleDragHelper.STATE_DRAGGING) {
                    pendingChange = PENDING_NONE
                }
            }

//            override fun getViewHorizontalDragRange(child: View, dx: Float, dy: Float): Int = monthViews[if (dx > 0) 0 else monthViews.size - 1]?.measuredWidth?.plus(calendarSpace) ?: 0

            override fun getViewVerticalDragRange(child: View, dx: Float, dy: Float): Int = 0

            fun getClampHorizontalDragRange(child: View, dx: Float, dy: Float): Int = monthViews[if (dx > 0) 0 else monthViews.size - 1]?.measuredWidth?.plus(calendarSpace) ?: 0

            fun getClampVerticalHeight(child: View, dx: Float, dy: Float): Int {
                val targetHeight = monthViews[if (dx > 0) 0 else monthViews.size - 1]?.measuredHeight ?: 0
                var maxHeight = 0
                if (calendarsCount > 1) {
                    val startOffset: Int = 2
                    val endOffset: Int = monthViews.size - 2
                    for (i in startOffset until endOffset) {
                        maxHeight = maxOf(maxHeight, monthViews[i]?.measuredHeight ?: 0)
                    }
                    if (dx > 0) {
                        maxHeight = maxOf(maxHeight, monthViews[1]?.measuredHeight ?: 0)
                    } else {
                        maxHeight = maxOf(maxHeight, monthViews[monthViews.size - 2]?.measuredHeight ?: 0)
                    }
                }
                return maxOf(targetHeight, maxHeight)
            }

        }
    }

}

class CalendarMonthView : ViewGroup {

    val dayViews by lazy {
        mutableListOf<CheckedTextView>()
    }

    val weekDayTitleViews by lazy {
        mutableListOf<TextView>()
    }

    var year = -1
        private set
    var month = -1
        private set
    var firstDayWeekOffset = 0
        private set
    var maxDaysInMonth = 0
        private set

    lateinit var monthTitleView: TextView

    private val calendar by lazy {
        GregorianCalendar()
    }

    private var isLayoutBlocked = false

    constructor(context: Context) : this(context, null)

    constructor(context: Context, attrs: AttributeSet?) : this(context, attrs, 0)

    constructor(context: Context, attrs: AttributeSet?, defStyle: Int) : super(context, attrs, defStyle)

    fun setup(parent: GoodCalendar, year: Int, month: Int) {
        calendar.set(year, month, 1)
        this.year = calendar.get(Calendar.YEAR)
        this.month = calendar.get(Calendar.MONTH)
        firstDayWeekOffset = calendar.get(GregorianCalendar.DAY_OF_WEEK) - 1

        maxDaysInMonth = calendar.getActualMaximum(GregorianCalendar.DAY_OF_MONTH)
        monthTitleView.text = parent.formatter.formatMonthTitle(calendar.time)
        dayViews.forEachIndexed { index, view ->
            if (index < maxDaysInMonth) {
                view.text = (index + 1).toString()
                if (view.isChecked) {
                    view.isChecked = false
                }
                view.isChecked = parent.mode.isChecked(year, month, index + 1)
                view.isEnabled = parent.isDateEnabled(year, month, index + 1)
                view.visibility = View.VISIBLE
            } else {
                view.text = ""
                view.isEnabled = false
                view.isChecked = false
                view.visibility = View.GONE
            }
        }
    }

    fun prepare(parent: GoodCalendar) {
        isLayoutBlocked = true
        addView(instantiateMonthTitleView(parent.monthTitleLayout).also {
            monthTitleView = it
        })

        (0 until 7).forEach {
            addView(instantiateWeekDayTitleView(parent.weekDayTitleLayout).also {
                weekDayTitleViews.add(it)
            })
        }

        (0 until 31).forEachIndexed { index, item ->
            addView(instantiateDayView(parent.dayLayout).also {
                it.setOnClickListener { view ->
                    parent.mode.onDayCheckChange(year, month, index + 1, it.isChecked)
                }
                if (it is CalendarDayView) {
                    it.prepareBackground(parent.tintColor)
                }
                dayViews.add(it)
            })
        }
        prepareWeekDayTitles(parent)

        isLayoutBlocked = false
        requestLayout()
        invalidate()
    }

    @SuppressLint("MissingSuperCall")
    override fun requestLayout() {
        if (!isLayoutBlocked) {
            super.requestLayout()
        }
    }

    override fun invalidate() {
        if (!isLayoutBlocked) {
            super.invalidate()
        }
    }

    override fun onLayout(changed: Boolean, l: Int, t: Int, r: Int, b: Int) {
        if (width == 0 || childCount == 0) return

        val left = paddingLeft
        var right = paddingRight
        var top = paddingTop

        val cellStep = width / 7

        val params = monthTitleView.layoutParams as CalendarMonthParams
        val gravity = GravityCompat.getAbsoluteGravity(params.gravity, ViewCompat.getLayoutDirection(this))
        when (gravity and Gravity.HORIZONTAL_GRAVITY_MASK) {
            Gravity.CENTER_HORIZONTAL -> {
                monthTitleView.layout(width / 2 - monthTitleView.measuredWidth / 2, top, width / 2 + monthTitleView.measuredWidth / 2, top + monthTitleView.measuredHeight)
            }
            Gravity.RIGHT,
            GravityCompat.END -> {
                monthTitleView.layout(paddingRight - monthTitleView.measuredWidth, top, paddingRight, top + monthTitleView.measuredHeight)
            }
            else -> {
                monthTitleView.layout(left, top, left + monthTitleView.measuredWidth, top + monthTitleView.measuredHeight)
            }
        }
        top += monthTitleView.measuredHeight

        (parent as? GoodCalendar)?.let {
            top += it.monthTitleSpace
        }

        var maxHeight = 0
        weekDayTitleViews.forEachIndexed { index, item ->
            item.layout(left + cellStep * index, top, left + cellStep * index + item.measuredWidth, top + item.measuredHeight)
            maxHeight = Math.max(maxHeight, item.measuredHeight)
        }
        top += maxHeight

        (parent as? GoodCalendar)?.let {
            top += it.weekDaysTitleSpace
        }

        maxHeight = 0
        var currentRow = -1
        dayViews.forEachIndexed { index, item ->
            if (item.visibility != View.GONE) {
                val index = (firstDayWeekOffset + index)
                val row = index / 7
                if (currentRow != row) {
                    top += maxHeight
                    currentRow = row
                    maxHeight = maxOf(maxHeight, item.measuredHeight)
                }
                item.layout(left + cellStep * (index % 7), top, left + cellStep * (index % 7) + item.measuredWidth, top + item.measuredHeight)
            }
        }
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {

        val availableWidth = MeasureSpec.getSize(widthMeasureSpec)
        val availableHeight = MeasureSpec.getSize(heightMeasureSpec)

        var preferredWidth = 0
        if (availableWidth > 0) {
            preferredWidth = (availableWidth - paddingLeft - paddingRight) / 7
        }

        var width = 0
        var height = 0
        var currentRow = -1
        var maxChildHeight = 0
        for (i in 0 until dayViews.size) {
            val view = dayViews[i]
            if (view.visibility == View.GONE) continue
            measureChildWithPreferredSize(view, widthMeasureSpec, heightMeasureSpec, preferredWidth)
            val row = (i + firstDayWeekOffset) / 7
            if (currentRow != row && view.measuredHeight > 0) {
                currentRow = row
                height += view.measuredHeight
                maxChildHeight = maxOf(maxChildHeight, view.measuredHeight)
            }
        }
        weekDayTitleViews.forEachIndexed { index, view ->
            measureChildWithPreferredSize(view, widthMeasureSpec, heightMeasureSpec, preferredWidth, 0)
            if (index == 0) {
                height += view.measuredHeight
            }
            width += view.measuredWidth
        }
//        val availableTitleWidth = if (parent is GoodCalendar) {
//            availableWidth - (parent as GoodCalendar).leftArrow!!.measuredWidth - (parent as GoodCalendar).rightArrow!!.measuredWidth
//        } else availableWidth
        measureChildWithPreferredSize(monthTitleView, widthMeasureSpec, heightMeasureSpec, availableWidth, 0)
        height += monthTitleView.measuredHeight

        (parent as? GoodCalendar)?.let {
            height += it.monthTitleSpace + it.weekDaysTitleSpace
            if ((!it.isResizeAllowed || MeasureSpec.getMode(it.initialHeightMode!!) == MeasureSpec.EXACTLY) && currentRow + 1 < 6) {
                height += maxChildHeight
            }
        }

        setMeasuredDimension(maxOf(availableWidth, width), height)
    }

    private fun measureChildWithPreferredSize(child: View, widthMeasureSpec: Int, heightMeasureSpec: Int, preferredWidth: Int, preferredHeight: Int = preferredWidth) {
        val params = child.layoutParams
        val widthSpec = if (preferredWidth > 0) {
            MeasureSpec.makeMeasureSpec(preferredWidth, MeasureSpec.EXACTLY)
        } else {
            getChildMeasureSpec(widthMeasureSpec, 0, params.width)
        }
        val heightSpec = if (preferredHeight > 0) {
            MeasureSpec.makeMeasureSpec(preferredHeight, MeasureSpec.EXACTLY)
        } else {
            getChildMeasureSpec(heightMeasureSpec, 0, params.height)
        }
        child.measure(widthSpec, heightSpec)
    }

    override fun checkLayoutParams(p: LayoutParams?): Boolean = p is CalendarMonthParams

    override fun generateLayoutParams(p: LayoutParams?): LayoutParams = CalendarMonthParams(p)

    override fun generateLayoutParams(attrs: AttributeSet?): LayoutParams = CalendarMonthParams(context, attrs)

    override fun generateDefaultLayoutParams(): LayoutParams = CalendarMonthParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT, Gravity.CENTER)

    fun syncCheckedState(calendarView: GoodCalendar) {
        dayViews.forEachIndexed { index, item ->
            if (item.isChecked) {
                item.isChecked = false
            }
            item.isChecked = item.isEnabled && calendarView.mode.isChecked(year, month, index + 1)
            item.isEnabled = calendarView.isDateEnabled(year, month, index + 1)
        }
    }

    fun updateTintColor(calendarView: GoodCalendar, color: Int) {
        dayViews.forEach {
            if (it is CalendarDayView) {
                it.prepareBackground(color)
            }
        }
    }

    private fun prepareWeekDayTitles(parent: GoodCalendar) {
        weekDayTitleViews.forEachIndexed { index, view ->
            view.text = parent.formatter.formatWeekDayTitle(index + 1)
        }
    }

    private fun instantiateDayView(layoutRes: Int): CheckedTextView {
        return if (layoutRes != -1) LayoutInflater.from(context).inflate(layoutRes, this, false) as CheckedTextView else CalendarDayView(context)
    }

    private fun instantiateMonthTitleView(layoutRes: Int): TextView {
        return if (layoutRes != -1) LayoutInflater.from(context).inflate(layoutRes, this, false) as TextView else CalendarMonthTitleView(context)
    }

    private fun instantiateWeekDayTitleView(layoutRes: Int): TextView {
        return if (layoutRes != -1) LayoutInflater.from(context).inflate(layoutRes, this, false) as TextView else CalendarWeekDayTitleView(context)
    }

    fun isNextChecked(view: View): Boolean = isNextChecked(dayViews.indexOf(view))
    fun isNextChecked(index: Int) = index < maxDaysInMonth - 1 && (parent as GoodCalendar).mode.isChecked(year, month, index + 2)

    fun isPreviousChecked(view: View): Boolean = isPreviousChecked(dayViews.indexOf(view))
    fun isPreviousChecked(index: Int) = index > 0 && (parent as GoodCalendar).mode.isChecked(year, month, index)

    fun isFirstInRow(view: View): Boolean = isFirstInRow(dayViews.indexOf(view))
    fun isFirstInRow(index: Int) = (firstDayWeekOffset + index) % 7 == 0

    fun isLastInRow(view: View): Boolean = isLastInRow(dayViews.indexOf(view))
    fun isLastInRow(index: Int) = (firstDayWeekOffset + index) % 7 == 6

    fun isTopChecked(view: View): Boolean = isTopChecked(dayViews.indexOf(view))
    fun isTopChecked(index: Int) = index - 7 >= 0 && (parent as GoodCalendar).mode.isChecked(year, month, index - 6)

    fun isBottomChecked(view: View): Boolean = isBottomChecked(dayViews.indexOf(view))
    fun isBottomChecked(index: Int) = index + 7 < maxDaysInMonth && (parent as GoodCalendar).mode.isChecked(year, month, index + 8)

    fun buildDayPath(view: View, path: Path) {
        val index = dayViews.indexOf(view)

        val nextChecked = isNextChecked(index)
        val previousChecked = isPreviousChecked(index)
        val firstInRow = isFirstInRow(index)
        val lastInRow = isLastInRow(index)
        val topChecked = isTopChecked(index)
        val bottomChecked = isBottomChecked(index)

        if (previousChecked && !firstInRow) {
            path.addRect(0F, 0F, (view.width / 2).toFloat(), view.height.toFloat(), Path.Direction.CW)
        }
        if (nextChecked && !lastInRow) {
            path.addRect((view.width / 2).toFloat(), 0F, view.width.toFloat(), view.height.toFloat(), Path.Direction.CW)
        }
        if (topChecked) {
            path.addRect(0F, 0F, view.width.toFloat(), (view.height / 2).toFloat(), Path.Direction.CW)
        }
        if (bottomChecked) {
            path.addRect(0F, (view.height / 2).toFloat(), view.width.toFloat(), view.height.toFloat(), Path.Direction.CW)
        }

    }

    open class CalendarMonthParams : LayoutParams {

        var gravity = -1

        constructor(width: Int, height: Int, gravity: Int) : super(width, height) {
            this.gravity = gravity
        }

        constructor(source: LayoutParams?) : super(source) {
            if (source is CalendarMonthParams) {
                gravity = source.gravity
            }
        }

        constructor(c: Context, attrs: AttributeSet?) : super(c, attrs) {
            val a = c.obtainStyledAttributes(attrs, intArrayOf(android.R.attr.layout_gravity))
            gravity = a.getInt(0, gravity)
            a.recycle()
        }

    }

}

class CalendarDayView : AppCompatCheckedTextView {

    private val path by lazy { Path() }
    private val paint by lazy { Paint(Paint.ANTI_ALIAS_FLAG) }
    private val hotspotBounds = Rect()
    private var customBackground: StateListDrawable? = null

    private var currentColor = -1
        set(value) {
            field = value
            paint.color = value
        }

    constructor(context: Context) : this(context, null)

    constructor(context: Context, attrs: AttributeSet?) : this(context, attrs, 0)

    constructor(context: Context, attrs: AttributeSet?, defStyle: Int) : super(context, attrs, defStyle) {
        setLayerType(View.LAYER_TYPE_SOFTWARE, null)
    }

    fun prepareBackground(color: Int) {
        currentColor = color
        if (customBackground == null) {
            if (background == null) {
                customBackground = generateBackground(color,
                        minOf(75, resources.getInteger(android.R.integer.config_shortAnimTime)))
                setBackgroundDrawable(customBackground)
                return
            }
        }
        customBackground?.let {
            getDrawableForState(intArrayOf(android.R.attr.state_checked))?.let {
                if (it is ShapeDrawable) {
                    it.paint.color = color
                } else {
                    it.setColorFilter(color, PorterDuff.Mode.SRC_IN)
                }
            }
            getDrawableForState(intArrayOf(android.R.attr.state_pressed))?.let {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP && it is RippleDrawable) {
                    it.setColor(ColorStateList.valueOf(color))
                } else if (it is ShapeDrawable) {
                    it.paint.color = color
                } else {
                    it.setColorFilter(color, PorterDuff.Mode.SRC_IN)
                }
            }
        }
    }

    override fun onDraw(canvas: Canvas?) {
        if (canvas == null) return
        if (isChecked) {
            (parent as? CalendarMonthView)?.let {
                path.reset()
                it.buildDayPath(this, path)
                canvas.drawPath(path, paint)
            }
        }
        super.onDraw(canvas)
    }

    private fun getDrawableForState(currentState: IntArray): Drawable? {
        val getStateDrawableIndex = StateListDrawable::class.java.getMethod("getStateDrawableIndex", IntArray::class.java)
        val getStateDrawable = StateListDrawable::class.java.getMethod("getStateDrawable", Int::class.java);
        val index = getStateDrawableIndex.invoke(customBackground, currentState);
        return getStateDrawable.invoke(customBackground, index) as Drawable
    }

    private fun generateBackground(color: Int, fadeTime: Int): StateListDrawable {
        val drawable = StateListDrawable()
        drawable.setExitFadeDuration(fadeTime)
        drawable.addState(intArrayOf(android.R.attr.state_checked), generateCircleDrawable(color))
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            drawable.addState(intArrayOf(android.R.attr.state_pressed), generateRippleDrawable(color, hotspotBounds))
        } else {
            drawable.addState(intArrayOf(android.R.attr.state_pressed), generateCircleDrawable(color))
        }
        drawable.addState(intArrayOf(), generateCircleDrawable(Color.TRANSPARENT))
        return drawable
    }

    private fun generateCircleDrawable(color: Int): Drawable {
        val drawable = ShapeDrawable(OvalShape())
        drawable.paint.color = color
        return drawable
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    private fun generateRippleDrawable(color: Int, bounds: Rect): Drawable {
        val list = ColorStateList.valueOf(color)
        val mask = generateCircleDrawable(Color.WHITE)
        val rippleDrawable = RippleDrawable(list, null, mask)
        if (Build.VERSION.SDK_INT == Build.VERSION_CODES.LOLLIPOP) {
            rippleDrawable.bounds = bounds
        }
        if (Build.VERSION.SDK_INT == Build.VERSION_CODES.LOLLIPOP_MR1) {
            val center = (bounds.left + bounds.right) / 2
            rippleDrawable.setHotspotBounds(center, bounds.top, center, bounds.bottom)
        }
        return rippleDrawable
    }

    override fun onSizeChanged(w: Int, h: Int, oldw: Int, oldh: Int) {
        super.onSizeChanged(w, h, oldw, oldh)
        if (customBackground != null && Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            calculateBounds(w, h)
            val center = (hotspotBounds.left + hotspotBounds.right) / 2
            customBackground?.setHotspotBounds(center, hotspotBounds.top, center, hotspotBounds.bottom)
        }
    }

    private fun calculateBounds(width: Int, height: Int) {
        val radius = Math.min(height, width)
        val offset = Math.abs(height - width) / 2
        val circleOffset = if (Build.VERSION.SDK_INT == Build.VERSION_CODES.LOLLIPOP) offset / 2 else offset
        if (width >= height) {
            hotspotBounds.set(circleOffset, 0, radius + circleOffset, height)
        } else {
            hotspotBounds.set(0, circleOffset, width, radius + circleOffset)
        }
    }
}

class CalendarMonthTitleView : AppCompatTextView {

    constructor(context: Context) : this(context, null)

    constructor(context: Context, attrs: AttributeSet?) : this(context, attrs, 0)

    constructor(context: Context, attrs: AttributeSet?, defStyle: Int) : super(context, attrs, defStyle)

}

class CalendarWeekDayTitleView : AppCompatTextView {

    constructor(context: Context) : this(context, null)

    constructor(context: Context, attrs: AttributeSet?) : this(context, attrs, 0)

    constructor(context: Context, attrs: AttributeSet?, defStyle: Int) : super(context, attrs, defStyle)

}

open class DateFormatter {

    val calendar by lazy {
        Calendar.getInstance()
    }

    val defFormatter by lazy {
        SimpleDateFormat("LLLL yyyy", Locale.getDefault())
    }

    fun formatMonthTitle(date: Date): CharSequence = defFormatter.format(date)

    fun formatWeekDayTitle(dayOfWeek: Int): CharSequence {
        calendar.set(Calendar.DAY_OF_WEEK, dayOfWeek)
        return calendar.getDisplayName(Calendar.DAY_OF_WEEK, Calendar.SHORT, Locale.getDefault())
    }
}

data class SelectedDate(val year: Int, val month: Int, val day: Int) {

    companion object {
        val SAME = 0
        val GRATER = 1
        val LOWER = 2

        fun fromDate(date: Date?): SelectedDate? {
            if (date != null) {
                val calendar = Calendar.getInstance()
                calendar.time = date
                return SelectedDate(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH))
            }
            return null
        }
    }

    fun match(year: Int, month: Int, day: Int): Boolean {
        return this.year == year && this.month == month && this.day == day
    }

    fun compare(year: Int, month: Int, day: Int = -1): Int =
            when {
                this.year > year -> GRATER
                this.year < year -> LOWER
                this.month > month -> GRATER
                this.month < month -> LOWER
                day > -1 && this.day > day -> GRATER
                day > -1 && this.day < day -> LOWER
                else -> SAME
            }

    fun isGraterOrSame(year: Int, month: Int, day: Int): Boolean {
        val cmp = compare(year, month, day)
        return cmp == GRATER || cmp == SAME
    }

    fun isLowerOrSame(year: Int, month: Int, day: Int): Boolean {
        val cmp = compare(year, month, day)
        return cmp == LOWER || cmp == SAME
    }

}

abstract class SelectionMode(val calendarView: GoodCalendar) {

    abstract fun onDayCheckChange(year: Int, month: Int, day: Int, checked: Boolean): Boolean

    abstract fun isChecked(year: Int, month: Int, day: Int): Boolean

}

class SimpleMode(calendarView: GoodCalendar) : SelectionMode(calendarView) {

    private var currentSelectedDate: SelectedDate? = null

    override fun isChecked(year: Int, month: Int, day: Int): Boolean {
        return currentSelectedDate?.match(year, month, day) == true
    }

    override fun onDayCheckChange(year: Int, month: Int, day: Int, checked: Boolean): Boolean {
        if (!checked) {
            currentSelectedDate = SelectedDate(year, month, day)
            calendarView.syncCheckedState()
        } else {
            currentSelectedDate = null
        }
        return !checked
    }

}

class RangeMode(calendarView: GoodCalendar) : SelectionMode(calendarView) {

    private var startDate: SelectedDate? = null
    private var endDate: SelectedDate? = null

    override fun onDayCheckChange(year: Int, month: Int, day: Int, checked: Boolean): Boolean {
        if (startDate == null || endDate != null) {
            startDate = SelectedDate(year, month, day)
            endDate = null
            calendarView.syncCheckedState()
            return true
        }
        if (startDate != null) {
            if (startDate!!.match(year, month, day)) {
                startDate = null
                calendarView.syncCheckedState()
                return false
            }
            if (startDate!!.compare(year, month, day) == SelectedDate.LOWER) {
                endDate = startDate
                startDate = SelectedDate(year, month, day)
                calendarView.syncCheckedState()
                return true
            } else {
                endDate = SelectedDate(year, month, day)
                calendarView.syncCheckedState()
                return true
            }
        }
        return false
    }

    override fun isChecked(year: Int, month: Int, day: Int): Boolean {
        if (endDate == null) {
            return startDate?.match(year, month, day) == true
        }
        return startDate?.isGraterOrSame(year, month, day) == true && endDate!!.isLowerOrSame(year, month, day)
    }

}

class MultipleMode(calendarView: GoodCalendar) : SelectionMode(calendarView) {

    private val selectedDates by lazy {
        mutableListOf<SelectedDate>()
    }

    override fun onDayCheckChange(year: Int, month: Int, day: Int, checked: Boolean): Boolean {
        if (checked) {
            selectedDates.remove(SelectedDate(year, month, day))
            calendarView.syncCheckedState()
            return false
        }
        selectedDates.add(SelectedDate(year, month, day))
        calendarView.syncCheckedState()
        return true
    }

    override fun isChecked(year: Int, month: Int, day: Int): Boolean {
        selectedDates.forEach { if (it.match(year, month, day)) return true }
        return false
    }

}