package maestro.goodcalendar

import android.content.Context
import android.support.v4.view.MotionEventCompat
import android.support.v4.view.VelocityTrackerCompat
import android.support.v4.widget.ScrollerCompat
import android.util.Log
import android.view.MotionEvent
import android.view.VelocityTracker
import android.view.View
import android.view.ViewConfiguration
import android.view.animation.Interpolator
import java.util.*

/**
 * Created by a.barouski on 8/30/2017.
 */
class SimpleDragHelper(val context: Context, val view: View, val callback: SimpleDragHelperCallback) {

    companion object {

        val INVALID_POINTER = -1

        val EDGE_SIZE = 20 // dp

        val EDGE_LEFT = 1 shl 0
        val EDGE_RIGHT = 1 shl 1
        val EDGE_TOP = 1 shl 2
        val EDGE_BOTTOM = 1 shl 3

        val STATE_IDLE = 0
        val STATE_DRAGGING = 1
        val STATE_SETTLING = 2

        private val MIN_DISTANCE_FOR_FLING = 25 // dips

        private val BASE_SETTLE_DURATION = 256 // ms
        private val MAX_SETTLE_DURATION = 600 // ms

        const val TAG = "SimpleDragHelper"
    }

    private var mVelocityTracker: VelocityTracker? = null
    private val mMaxVelocity: Float
    private var mMinVelocity: Float

    private var mActivePointerId = INVALID_POINTER
    private var mInitialMotionX: FloatArray? = null
    private var mInitialMotionY: FloatArray? = null
    private var mLastMotionX: FloatArray? = null
    private var mLastMotionY: FloatArray? = null
    private var mInitialEdgesTouched: IntArray? = null
    private var mEdgeDragsInProgress: IntArray? = null
    private var mEdgeDragsLocked: IntArray? = null
    private var mPointersDown: Int = 0

    private val mEdgeSize: Int
    private var mTrackingEdges: Int = 0

    private var mCurrentDx = 0
    private var mCurrentDy = 0

    private var mFlingDistance = 0

    private val mScroller: ScrollerCompat

    // Current drag state; idle, dragging or settling
    var dragState: Int = 0
        private set

    private var mReleaseInProgress: Boolean = false

    private val sInterpolator = Interpolator { t ->
        var t = t
        t -= 1.0f
        t * t * t * t * t + 1.0f
    }

    private val mSetIdleRunnable = Runnable { setDragState(STATE_IDLE) }

    // Distance to travel before a drag may begin
    var mTouchSlop: Int = 0

    init {

        val vc = ViewConfiguration.get(context)
        val density = context.resources.displayMetrics.density
        mEdgeSize = (EDGE_SIZE * density + 0.5f).toInt()

        mTouchSlop = vc.scaledTouchSlop
        mMaxVelocity = vc.scaledMaximumFlingVelocity.toFloat()
        mMinVelocity = vc.scaledMinimumFlingVelocity.toFloat()
        mScroller = ScrollerCompat.create(context, sInterpolator)

        mFlingDistance = (MIN_DISTANCE_FOR_FLING * density).toInt()
    }

    fun shouldInterceptTouchEvent(ev: MotionEvent): Boolean {
        val action = MotionEventCompat.getActionMasked(ev)
        val actionIndex = MotionEventCompat.getActionIndex(ev)

        if (action == MotionEvent.ACTION_DOWN) {
            // Reset things for a new event stream, just in case we didn't get
            // the whole previous stream.
            cancel()
        }

        if (mVelocityTracker == null) {
            mVelocityTracker = VelocityTracker.obtain()
        }
        mVelocityTracker?.addMovement(ev)

        when (action) {
            MotionEvent.ACTION_DOWN -> {
                val x = ev.x
                val y = ev.y
                val pointerId = ev.getPointerId(0)
                saveInitialMotion(x, y, pointerId)

                // Catch a settling view if possible.
                if (dragState == STATE_SETTLING) {
                    tryCaptureViewForDrag(pointerId)
                }

                val edgesTouched = mInitialEdgesTouched!![pointerId]
                if (edgesTouched and mTrackingEdges != 0) {
                    callback.onEdgeTouched(edgesTouched and mTrackingEdges, pointerId)
                }
            }

            MotionEventCompat.ACTION_POINTER_DOWN -> {
                val pointerId = ev.getPointerId(actionIndex)
                val x = ev.getX(actionIndex)
                val y = ev.getY(actionIndex)

                saveInitialMotion(x, y, pointerId)

                // A DragHelper can only manipulate one view at a time.
                if (dragState == STATE_IDLE) {
                    val edgesTouched = mInitialEdgesTouched!![pointerId]
                    if (edgesTouched and mTrackingEdges != 0) {
                        callback.onEdgeTouched(edgesTouched and mTrackingEdges, pointerId)
                    }
                } else if (dragState == STATE_SETTLING) {
                    tryCaptureViewForDrag(pointerId)
                }
            }

            MotionEvent.ACTION_MOVE -> {
                if (mInitialMotionX != null && mInitialMotionY != null) {
                    // First to cross a touch slop over a draggable view wins. Also report edge drags.
                    val pointerCount = ev.pointerCount
                    for (i in 0 until pointerCount) {
                        val pointerId = ev.getPointerId(i)

                        // If pointer is invalid then skip the ACTION_MOVE.
                        if (!isValidPointerForActionMove(pointerId)) continue

                        val x = ev.getX(i)
                        val y = ev.getY(i)
                        val dx = x - mInitialMotionX!![pointerId]
                        val dy = y - mInitialMotionY!![pointerId]

                        val pastSlop = checkTouchSlop(view, dx, dy)
                        if (pastSlop) {
                            // check the callback's
                            // getView[Horizontal|Vertical]DragRange methods to know
                            // if you can move at all along an axis, then see if it
                            // would clamp to the same value. If you can't move at
                            // all in every dimension with a nonzero range, bail.
                            val horizontalDragRange = callback.getViewHorizontalDragRange(view, dx, dy)
                            val verticalDragRange = callback.getViewVerticalDragRange(view, dx, dy)

                            if ((horizontalDragRange == 0 || horizontalDragRange > 0 && dx == 0F) && (verticalDragRange == 0 || verticalDragRange > 0 && dy == 0F)) {
                                break
                            }
                        }
                        reportNewEdgeDrags(dx, dy, pointerId)
                        if (dragState == STATE_DRAGGING) {
                            // Callback might have started an edge drag
                            break
                        }

                        if (pastSlop
                                && callback.canScrollHorizontally(view, dx, x, y)
                                && callback.canScrollVertically(view, dy, x, y)
                                && tryCaptureViewForDrag(pointerId)) {
                            break
                        }
                    }
                    saveLastMotion(ev)
                }
            }

            MotionEventCompat.ACTION_POINTER_UP -> {
                val pointerId = ev.getPointerId(actionIndex)
                clearMotionHistory(pointerId)
            }

            MotionEvent.ACTION_UP, MotionEvent.ACTION_CANCEL -> {
                cancel()
            }
        }
        return dragState == STATE_DRAGGING
    }

    fun processTouchEvent(ev: MotionEvent) {
        val action = MotionEventCompat.getActionMasked(ev)
        val actionIndex = MotionEventCompat.getActionIndex(ev)

        if (action == MotionEvent.ACTION_DOWN) {
            // Reset things for a new event stream, just in case we didn't get
            // the whole previous stream.
            cancel()
        }

        if (mVelocityTracker == null) {
            mVelocityTracker = VelocityTracker.obtain()
        }
        mVelocityTracker!!.addMovement(ev)

        when (action) {
            MotionEvent.ACTION_DOWN -> {
                val x = ev.x
                val y = ev.y
                val pointerId = ev.getPointerId(0)

                saveInitialMotion(x, y, pointerId)

                // Since the parent is already directly processing this touch event,
                // there is no reason to delay for a slop before dragging.
                // Start immediately if possible.
                tryCaptureViewForDrag(pointerId)

                val edgesTouched = mInitialEdgesTouched!![pointerId]
                if (edgesTouched and mTrackingEdges != 0) {
                    callback.onEdgeTouched(edgesTouched and mTrackingEdges, pointerId)
                }
            }

            MotionEventCompat.ACTION_POINTER_DOWN -> {
                val pointerId = ev.getPointerId(actionIndex)
                val x = ev.getX(actionIndex)
                val y = ev.getY(actionIndex)

                saveInitialMotion(x, y, pointerId)

                // A DragHelper can only manipulate one view at a time.
                if (dragState == STATE_IDLE) {
                    // If we're idle we can do anything! Treat it like a normal down event.

                    tryCaptureViewForDrag(pointerId)

                    val edgesTouched = mInitialEdgesTouched!![pointerId]
                    if (edgesTouched and mTrackingEdges != 0) {
                        callback.onEdgeTouched(edgesTouched and mTrackingEdges, pointerId)
                    }
                } else if (isCapturedViewUnder(x.toInt(), y.toInt())) {
                    // We're still tracking a captured view. If the same view is under this
                    // point, we'll swap to controlling it with this pointer instead.
                    // (This will still work if we're "catching" a settling view.)

                    tryCaptureViewForDrag(pointerId)
                }
            }

            MotionEvent.ACTION_MOVE -> {
                if (dragState == STATE_DRAGGING) {
                    // If pointer is invalid then skip the ACTION_MOVE.
                    if (isValidPointerForActionMove(mActivePointerId)) {

                        val index = ev.findPointerIndex(mActivePointerId)
                        val x = ev.getX(index)
                        val y = ev.getY(index)
                        val idx = (x - mLastMotionX!![mActivePointerId]).toInt()
                        val idy = (y - mLastMotionY!![mActivePointerId]).toInt()
                        val newDx = mCurrentDx + idx
                        val newDy = mCurrentDy + idy

                        val horizontalRange = callback.getViewHorizontalDragRange(view, newDx.toFloat(), newDy.toFloat())
                        val verticalRange = callback.getViewVerticalDragRange(view, newDx.toFloat(), newDy.toFloat())

//                        val haveDifferentSize =  callback.getViewHorizontalDragRange(view, newDx.toFloat(), newDy.toFloat())!= callback.getViewHorizontalDragRange(view, mCurrentDx.toFloat(), mCurrentDy.toFloat())
//                        if (newDx > 0F && mCurrentDx < 0F && haveDifferentSize) {
//                            newDx = 0
//                        }
//                        if (newDy > 0F && mCurrentDy < 0F && haveDifferentSize) {
//                            newDy = 0
//                        }

                        mCurrentDx = minOf(Math.abs(newDx), horizontalRange)
                        if (newDx < 0) {
                            mCurrentDx *= -1
                        }
                        mCurrentDy = minOf(Math.abs(newDy), verticalRange)
                        if (newDy < 0) {
                            mCurrentDy *= -1
                        }

                        dragTo(mCurrentDx, mCurrentDy, idx, idy)

                        saveLastMotion(ev)
                    }
                } else {
                    // Check to see if any pointer is now over a draggable view.
                    val pointerCount = ev.pointerCount
                    for (i in 0 until pointerCount) {
                        val pointerId = ev.getPointerId(i)

                        // If pointer is invalid then skip the ACTION_MOVE.
                        if (!isValidPointerForActionMove(pointerId)) continue

                        val x = ev.getX(i)
                        val y = ev.getY(i)
                        val dx = x - mInitialMotionX!![pointerId]
                        val dy = y - mInitialMotionY!![pointerId]

                        reportNewEdgeDrags(dx, dy, pointerId)
                        if (dragState == STATE_DRAGGING) {
                            // Callback might have started an edge drag.
                            break
                        }

                        if (checkTouchSlop(view, dx, dy) && tryCaptureViewForDrag(pointerId)) {
                            break
                        }
                    }
                    saveLastMotion(ev)
                }
            }

            MotionEventCompat.ACTION_POINTER_UP -> {
                val pointerId = ev.getPointerId(actionIndex)
                if (dragState == STATE_DRAGGING && pointerId == mActivePointerId) {
                    // Try to find another pointer that's still holding on to the captured view.
                    var newActivePointer = INVALID_POINTER
                    val pointerCount = ev.pointerCount
                    for (i in 0 until pointerCount) {
                        val id = ev.getPointerId(i)
                        if (id == mActivePointerId) {
                            // This one's going away, skip.
                            continue
                        }
                        if (tryCaptureViewForDrag(id)) {
                            newActivePointer = mActivePointerId
                            break
                        }
                    }

                    if (newActivePointer == INVALID_POINTER) {
                        // We didn't find another pointer still touching the view, release it.
                        releaseViewForPointerUp()
                    }
                }
                clearMotionHistory(pointerId)
            }

            MotionEvent.ACTION_UP -> {
                if (dragState == STATE_DRAGGING) {
                    releaseViewForPointerUp()
                }
                cancel()
            }

            MotionEvent.ACTION_CANCEL -> {
                if (dragState == STATE_DRAGGING) {
                    dispatchViewReleased(0f, 0f)
                }
                cancel()
            }
        }
    }

    private fun releaseViewForPointerUp() {
        mVelocityTracker?.computeCurrentVelocity(1000, mMaxVelocity)
        val xvel = clampMag(
                VelocityTrackerCompat.getXVelocity(mVelocityTracker, mActivePointerId),
                mMinVelocity, mMaxVelocity)
        val yvel = clampMag(
                VelocityTrackerCompat.getYVelocity(mVelocityTracker, mActivePointerId),
                mMinVelocity, mMaxVelocity)
        dispatchViewReleased(xvel, yvel)
    }

    fun forceSetDxDy(dx: Int, dy: Int) {
        mCurrentDx = dx
        mCurrentDy = dy
    }

    fun settleCapturedViewAt(currentLeft: Int, currentTop: Int, finalLeft: Int, finalTop: Int): Boolean {
        if (!mReleaseInProgress) {
            throw IllegalStateException("Cannot settleCapturedViewAt outside of a call to " + "Callback#onViewReleased")
        }

        return forceSettleCapturedViewAt(currentLeft, currentTop, finalLeft, finalTop,
                VelocityTrackerCompat.getXVelocity(mVelocityTracker, mActivePointerId).toInt(),
                VelocityTrackerCompat.getYVelocity(mVelocityTracker, mActivePointerId).toInt())
    }

    fun animateSettle(currentLeft: Int, currentTop: Int, finalLeft: Int, finalTop: Int): Boolean {
        if (dragState != STATE_IDLE) return false
        return forceSettleCapturedViewAt(currentLeft, currentTop, finalLeft, finalTop, 0, 0)
    }

    /**
     * Settle the captured view at the given (left, top) position.
     *
     * @param finalLeft Target left position for the captured view
     * @param finalTop  Target top position for the captured view
     * @param xvel      Horizontal velocity
     * @param yvel      Vertical velocity
     * @return true if animation should continue through [.continueSettling] calls
     */
    private fun forceSettleCapturedViewAt(currentLeft: Int, currentTop: Int, finalLeft: Int, finalTop: Int, xvel: Int, yvel: Int): Boolean {

        val dx = finalLeft - currentLeft
        val dy = finalTop - currentTop

        Log.e(TAG, "forceSettleCapturedViewAt: $currentLeft -> $finalLeft")

        if (dx == 0 && dy == 0) {
            // Nothing to do. Send callbacks, be done.
            mScroller.abortAnimation()
            setDragState(STATE_IDLE)
            return false
        }

        val duration = computeSettleDuration(view, dx, dy, xvel, yvel)
        mScroller.startScroll(currentLeft, currentTop, dx, dy, duration)

        setDragState(STATE_SETTLING)
        return true
    }

    private fun computeSettleDuration(child: View, dx: Int, dy: Int, xvel: Int, yvel: Int): Int {
        var xvel = xvel
        var yvel = yvel
        xvel = clampMag(xvel, mMinVelocity.toInt(), mMaxVelocity.toInt())
        yvel = clampMag(yvel, mMinVelocity.toInt(), mMaxVelocity.toInt())
        val absDx = Math.abs(dx)
        val absDy = Math.abs(dy)
        val absXVel = Math.abs(xvel)
        val absYVel = Math.abs(yvel)
        val addedVel = absXVel + absYVel
        val addedDistance = absDx + absDy

        val xweight = if (xvel != 0)
            absXVel.toFloat() / addedVel
        else
            absDx.toFloat() / addedDistance
        val yweight = if (yvel != 0)
            absYVel.toFloat() / addedVel
        else
            absDy.toFloat() / addedDistance

        val xduration = computeAxisDuration(dx, xvel, callback.getViewHorizontalDragRange(child, dx.toFloat(), dy.toFloat()))
        val yduration = computeAxisDuration(dy, yvel, callback.getViewVerticalDragRange(child, dx.toFloat(), dy.toFloat()))

        return (xduration * xweight + yduration * yweight).toInt()
    }

    private fun computeAxisDuration(delta: Int, velocity: Int, motionRange: Int): Int {
        var velocity = velocity
        if (delta == 0) {
            return 0
        }

        val width = view.getWidth()
        val halfWidth = width / 2
        val distanceRatio = Math.min(1f, Math.abs(delta).toFloat() / width)
        val distance = halfWidth + halfWidth * distanceInfluenceForSnapDuration(distanceRatio)

        val duration: Int
        velocity = Math.abs(velocity)
        if (velocity > 0) {
            duration = (4 * Math.round(1000 * Math.abs(distance / velocity))).toInt()
        } else {
            val range = Math.abs(delta).toFloat() / motionRange
            duration = ((range + 1) * BASE_SETTLE_DURATION).toInt()
        }
        return Math.min(duration, MAX_SETTLE_DURATION)
    }

    private fun clampMag(value: Int, absMin: Int, absMax: Int): Int {
        val absValue = Math.abs(value)
        if (absValue < absMin) return 0
        return if (absValue > absMax) if (value > 0) absMax else -absMax else value
    }

    private fun distanceInfluenceForSnapDuration(f: Float): Float {
        var f = f
        f -= 0.5f // center the values about 0.
        f *= (0.3f * Math.PI / 2.0f).toFloat()
        return Math.sin(f.toDouble()).toFloat()
    }

    /**
     * Clamp the magnitude of value for absMin and absMax.
     * If the value is below the minimum, it will be clamped to zero.
     * If the value is above the maximum, it will be clamped to the maximum.
     *
     * @param value  Value to clamp
     * @param absMin Absolute value of the minimum significant value to return
     * @param absMax Absolute value of the maximum value to return
     * @return The clamped value with the same sign as `value`
     */
    private fun clampMag(value: Float, absMin: Float, absMax: Float): Float {
        val absValue = Math.abs(value)
        if (absValue < absMin) return 0f
        return if (absValue > absMax) if (value > 0) absMax else -absMax else value
    }

    private fun dragTo(dx: Int, dy: Int, idx: Int, idy: Int) {
        if (idx != 0 || idy != 0) {
            callback.onPositionChanged(view, dx, dy, idx, idy)
        }
    }

    fun isCapturedViewUnder(x: Int, y: Int): Boolean {
        return isViewUnder(x, y)
    }

    private fun dispatchViewReleased(xvel: Float, yvel: Float) {
        mReleaseInProgress = true
        callback.onViewReleased(view,
                mCurrentDx,
                mCurrentDy,
                if (Math.abs(mCurrentDx) > mFlingDistance) xvel else 0F,
                if (Math.abs(mCurrentDy) > mFlingDistance) yvel else 0F)
        mReleaseInProgress = false

        if (dragState == STATE_DRAGGING) {
            // onViewReleased didn't call a method that would have changed this. Go idle.
            setDragState(STATE_IDLE)
        }
    }

    /**
     * Determine if the supplied view is under the given point in the
     * parent view's coordinate system.
     *
     * @param view Child view of the parent to hit test
     * @param x    X position to test in the parent's coordinate system
     * @param y    Y position to test in the parent's coordinate system
     * @return true if the supplied view is under the given point, false otherwise
     */
    fun isViewUnder(x: Int, y: Int): Boolean {
        return x >= viewLeft() && x < viewRight() && y >= viewTop() && y < viewBottom()
    }

    internal fun tryCaptureViewForDrag(pointerId: Int): Boolean {
        if (mActivePointerId == pointerId) {
            // Already done!
            return true
        }
        mActivePointerId = pointerId
        setDragState(STATE_DRAGGING)
        return true
    }

    internal fun setDragState(state: Int) {
        view.removeCallbacks(mSetIdleRunnable)
        if (dragState != state) {
            dragState = state
            callback.onViewDragStateChanged(state)
            if (dragState == STATE_IDLE) {
                mCurrentDx = 0
                mCurrentDy = 0
            }
        }
    }

    fun abort() {
        cancel()
        if (dragState == STATE_SETTLING) {
            val oldX = mScroller.currX
            val oldY = mScroller.currY
            mScroller.abortAnimation()
            val newX = mScroller.currX
            val newY = mScroller.currY
            callback.onPositionChanged(view, newX, newY, newX - oldX, newY - oldY)
        }
        setDragState(STATE_IDLE)
    }

    fun continueSettling(deferCallbacks: Boolean): Boolean {
        if (dragState == STATE_SETTLING) {
            var keepGoing = mScroller.computeScrollOffset()
            val x = mScroller.currX
            val y = mScroller.currY
            val dx = x - mCurrentDx
            val dy = y - mCurrentDy
            mCurrentDx = x
            mCurrentDy = y

            if (dx != 0 || dy != 0) {
                callback.onPositionChanged(view, mCurrentDx, mCurrentDy, dx, dy)
            }

            if (keepGoing && x == mScroller.finalX && y == mScroller.finalY) {
                // Close enough. The interpolator/scroller might think we're still moving
                // but the user sure doesn't.
                mScroller.abortAnimation()
                keepGoing = false
            }

            if (!keepGoing) {
                if (deferCallbacks) {
                    view.post(mSetIdleRunnable)
                } else {
                    setDragState(STATE_IDLE)
                }
            }
        }

        return dragState == STATE_SETTLING
    }

    private fun reportNewEdgeDrags(dx: Float, dy: Float, pointerId: Int) {
        var dragsStarted = 0
        if (checkNewEdgeDrag(dx, dy, pointerId, EDGE_LEFT)) {
            dragsStarted = dragsStarted or EDGE_LEFT
        }
        if (checkNewEdgeDrag(dy, dx, pointerId, EDGE_TOP)) {
            dragsStarted = dragsStarted or EDGE_TOP
        }
        if (checkNewEdgeDrag(dx, dy, pointerId, EDGE_RIGHT)) {
            dragsStarted = dragsStarted or EDGE_RIGHT
        }
        if (checkNewEdgeDrag(dy, dx, pointerId, EDGE_BOTTOM)) {
            dragsStarted = dragsStarted or EDGE_BOTTOM
        }

        if (dragsStarted != 0) {
            mEdgeDragsInProgress!![pointerId] = mEdgeDragsInProgress!![pointerId] or dragsStarted
            callback.onEdgeDragStarted(dragsStarted, pointerId)
        }
    }

    private fun checkNewEdgeDrag(delta: Float, odelta: Float, pointerId: Int, edge: Int): Boolean {
        val absDelta = Math.abs(delta)
        val absODelta = Math.abs(odelta)

        if (mInitialEdgesTouched!![pointerId] and edge != edge || mTrackingEdges and edge == 0
                || mEdgeDragsLocked!![pointerId] and edge == edge
                || mEdgeDragsInProgress!![pointerId] and edge == edge
                || absDelta <= mTouchSlop && absODelta <= mTouchSlop) {
            return false
        }
        if (absDelta < absODelta * 0.5f && callback.onEdgeLock(edge)) {
            mEdgeDragsLocked!![pointerId] = mEdgeDragsLocked!![pointerId] or edge
            return false
        }
        return mEdgeDragsInProgress!![pointerId] and edge == 0 && absDelta > mTouchSlop
    }

    private fun saveLastMotion(ev: MotionEvent) {
        val pointerCount = ev.pointerCount
        for (i in 0 until pointerCount) {
            val pointerId = ev.getPointerId(i)
            // If pointer is invalid then skip saving on ACTION_MOVE.
            if (!isValidPointerForActionMove(pointerId)) {
                continue
            }
            val x = ev.getX(i)
            val y = ev.getY(i)
            mLastMotionX!![pointerId] = x
            mLastMotionY!![pointerId] = y
        }
    }

    private fun isValidPointerForActionMove(pointerId: Int): Boolean {
        return isPointerDown(pointerId)
    }

    private fun isPointerDown(pointerId: Int): Boolean {
        return mPointersDown and (1 shl pointerId) != 0
    }

    fun cancel() {
        mActivePointerId = INVALID_POINTER
        clearMotionHistory()

        if (mVelocityTracker != null) {
            mVelocityTracker?.recycle()
            mVelocityTracker = null
        }
    }


    private fun clearMotionHistory() {
        if (mInitialMotionX == null) {
            return
        }
        Arrays.fill(mInitialMotionX, 0f)
        Arrays.fill(mInitialMotionY, 0f)
        Arrays.fill(mLastMotionX, 0f)
        Arrays.fill(mLastMotionY, 0f)
        Arrays.fill(mInitialEdgesTouched, 0)
        Arrays.fill(mEdgeDragsInProgress, 0)
        Arrays.fill(mEdgeDragsLocked, 0)
        mPointersDown = 0
    }

    private fun clearMotionHistory(pointerId: Int) {
        if (mInitialMotionX == null || !isPointerDown(pointerId)) {
            return
        }
        mInitialMotionX!![pointerId] = 0f
        mInitialMotionY!![pointerId] = 0f
        mLastMotionX!![pointerId] = 0f
        mLastMotionY!![pointerId] = 0f
        mInitialEdgesTouched!![pointerId] = 0
        mEdgeDragsInProgress!![pointerId] = 0
        mEdgeDragsLocked!![pointerId] = 0
        mPointersDown = mPointersDown and (1 shl pointerId).inv()
    }

    private fun saveInitialMotion(x: Float, y: Float, pointerId: Int) {
        ensureMotionHistorySizeForId(pointerId)
        mLastMotionX!![pointerId] = x
        mInitialMotionX!![pointerId] = mLastMotionX!![pointerId]
        mLastMotionY!![pointerId] = y
        mInitialMotionY!![pointerId] = mLastMotionY!![pointerId]
        mInitialEdgesTouched!![pointerId] = getEdgesTouched(x.toInt(), y.toInt())
        mPointersDown = mPointersDown or (1 shl pointerId)
    }

    private fun ensureMotionHistorySizeForId(pointerId: Int) {
        if (mInitialMotionX == null || mInitialMotionX!!.size <= pointerId) {
            val imx = FloatArray(pointerId + 1)
            val imy = FloatArray(pointerId + 1)
            val lmx = FloatArray(pointerId + 1)
            val lmy = FloatArray(pointerId + 1)
            val iit = IntArray(pointerId + 1)
            val edip = IntArray(pointerId + 1)
            val edl = IntArray(pointerId + 1)

            if (mInitialMotionX != null) {
                System.arraycopy(mInitialMotionX, 0, imx, 0, mInitialMotionX!!.size)
                System.arraycopy(mInitialMotionY, 0, imy, 0, mInitialMotionY!!.size)
                System.arraycopy(mLastMotionX, 0, lmx, 0, mLastMotionX!!.size)
                System.arraycopy(mLastMotionY, 0, lmy, 0, mLastMotionY!!.size)
                System.arraycopy(mInitialEdgesTouched, 0, iit, 0, mInitialEdgesTouched!!.size)
                System.arraycopy(mEdgeDragsInProgress, 0, edip, 0, mEdgeDragsInProgress!!.size)
                System.arraycopy(mEdgeDragsLocked, 0, edl, 0, mEdgeDragsLocked!!.size)
            }

            mInitialMotionX = imx
            mInitialMotionY = imy
            mLastMotionX = lmx
            mLastMotionY = lmy
            mInitialEdgesTouched = iit
            mEdgeDragsInProgress = edip
            mEdgeDragsLocked = edl
        }
    }


    private fun getEdgesTouched(x: Int, y: Int): Int {
        var result = 0

        if (x < viewLeft() + mEdgeSize) result = result or EDGE_LEFT
        if (y < viewTop() + mEdgeSize) result = result or EDGE_TOP
        if (x > viewRight() - mEdgeSize) result = result or EDGE_RIGHT
        if (y > viewBottom() - mEdgeSize) result = result or EDGE_BOTTOM

        return result
    }

    fun viewLeft(): Int {
        return view.translationX.toInt()// + view.getLeft();
    }

    fun viewTop(): Int {
        return view.translationY.toInt()// + view.getTop();
    }

    fun viewRight(): Int {
        return viewLeft() + view.measuredWidth
    }

    fun viewBottom(): Int {
        return viewTop() + view.measuredHeight
    }

    private fun checkTouchSlop(child: View?, dx: Float, dy: Float): Boolean {
        if (child == null) {
            return false
        }
        val checkHorizontal = callback.getViewHorizontalDragRange(child, dx, dy) > 0
        val checkVertical = callback.getViewVerticalDragRange(child, dx, dy) > 0

        if (checkHorizontal && checkVertical) {
            return dx * dx + dy * dy > mTouchSlop * mTouchSlop
        } else if (checkHorizontal) {
            return Math.abs(dx) > mTouchSlop
        } else if (checkVertical) {
            return Math.abs(dy) > mTouchSlop
        }
        return false
    }

    open class SimpleDragHelperCallback {

        open fun getViewHorizontalDragRange(child: View, dx: Float, dy: Float): Int {
            return child.width
        }

        open fun getViewVerticalDragRange(child: View, dx: Float, dy: Float): Int {
            return child.height
        }

        open fun onEdgeLock(edgeFlags: Int): Boolean {
            return false
        }

        open fun onEdgeDragStarted(edgeFlags: Int, pointerId: Int) {}

        open fun onViewDragStateChanged(state: Int) {}

        open fun onEdgeTouched(edgeFlags: Int, pointerId: Int) {}

        open fun canScrollHorizontally(view: View, dx: Float, xTouch: Float, yTouch: Float): Boolean {
            return true
        }

        open fun canScrollVertically(view: View, dy: Float, xTouch: Float, yTouch: Float): Boolean {
            return true
        }

        open fun onViewReleased(releasedChild: View, dx: Int, dy: Int, xvel: Float, yvel: Float) {}

        open fun onPositionChanged(changedView: View, dx: Int, dy: Int, idx: Int, idy: Int) {}

    }

}