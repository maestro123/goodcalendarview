package wtg.maestro.goodcalendar

import android.graphics.Color
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*
import maestro.goodcalendar.RangeMode
import java.util.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

//        month_view.dayViews.forEach {
//            it.text = "1"
//        }
//
//        month_view.monthTitleView.text = "Test month title"
//
//        month_view.weekDayTitleViews.forEach {
//            it.text = "D"
//        }

        calendar_view.tintColor = Color.RED
        calendar_view.mode = RangeMode(calendar_view)
//        calendar_view.setLimits(Date().also {
//            it.month -= 1
//        }, Date())
//
        btn_next.setOnClickListener { calendar_view.jumpNext() }
        btn_previous.setOnClickListener { calendar_view.jumpPrevious() }
    }
}
